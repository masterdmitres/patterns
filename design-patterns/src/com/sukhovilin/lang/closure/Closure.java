package com.sukhovilin.lang.closure;

public interface Closure<T> {

	void apply(T input);
	
}
