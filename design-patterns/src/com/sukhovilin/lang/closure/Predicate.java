package com.sukhovilin.lang.closure;

public interface Predicate<T> {
	
	boolean apply(T input);
	
}
